package com.example.app;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author luca
 */
public class Utils {

    public static final String EMAIL = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

    public static String toString(Map m) {
//        StringBuffer sb = new StringBuffer();
//        if (!isNullOrEmpty(m))
//        return String.format("{%s}", sb);
        return "[]";
    }

    public static String toString(Collection c) {
        return !isNullOrEmpty(c) ? c.stream().map(Object::toString).collect(Collectors.joining("],[", "[", "]")).toString() : "[]";
    }

    public static boolean isNotNullOrEmpty(String s) {
        return !isNullOrEmpty(s);
    }

    public static int count(Map m) {
        return isNullOrEmpty(m) ? 0 : m.size();
    }

    public static int count(Collection c) {
        return isNullOrEmpty(c) ? 0 : c.size();
    }

    private Utils() {
    }

    public static String getLogPrefix(String id) {
        return String.format("[sessUID=%s] ", id);
    }

    public static boolean isEmailAddress(String s) {
        return !isNullOrEmpty(s) && s.matches(EMAIL);
    }

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }

    public static boolean isNullOrEmpty(Map m) {
        return m == null || m.isEmpty();
    }

    public static boolean isNullOrEmpty(Collection s) {
        return s == null || s.isEmpty();
    }
}
