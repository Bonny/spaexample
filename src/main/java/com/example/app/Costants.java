package com.example.app;

/**
 *
 * @author luca
 */
public final class Costants {
    
    private Costants() {}
    
    public static final String APP_NAME = "SPAExample";

    public static final String APP_REL_VER = "1.0.0";

    public static final String APP_REL_DATE = "7/05/2018";
    
    public static final String DISPLAY_NAME = String.format("%s v%s - %s", Costants.APP_NAME, Costants.APP_REL_VER, Costants.APP_REL_DATE);

    public static final String DISPLAY_TITLE = String.format("%s v%s", Costants.APP_NAME, Costants.APP_REL_VER);
}
