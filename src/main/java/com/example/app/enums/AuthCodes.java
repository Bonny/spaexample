package com.example.app.enums;

/**
 *
 * @author luca
 */
public enum AuthCodes {
    SUCCESS,
    ERROR,
    NOT_FOUND,
    NOT_ACTIVE,
}
