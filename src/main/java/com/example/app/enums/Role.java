package com.example.app.enums;

/**
 *
 * @author luca
 */
public enum Role {
    ADMIN, 
    EDITOR,
    NORMAL
}
