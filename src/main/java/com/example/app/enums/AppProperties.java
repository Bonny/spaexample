package com.example.app.enums;

public enum AppProperties {

    /**
     * 
     */
    LOG_CONFIG("log.file.properties"),
    /**
     * 
     */
    TOKEN_TS_EXPIRATION("token.ts.expiration", "60")//60m = 1h
    ;

    private String key;
    private String def;

    private AppProperties(String key) {
        this.key = key;
        this.def = "";
    }

    private AppProperties(String key, String def) {
        this.key = key;
        this.def = def;
    }

    public String getKey() {
        return key;
    }

    public String getDef() {
        return def;
    }

}
