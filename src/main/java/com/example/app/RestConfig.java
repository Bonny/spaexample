package com.example.app;

import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author luca
 */
@ApplicationPath("/rest")
public class RestConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.example.app.filters.AuthenticationFilter.class);
        resources.add(com.example.app.filters.AuthorizationFilter.class);
        resources.add(com.example.app.filters.CorsFilter.class);
        resources.add(com.example.app.filters.LogFilter.class);
        resources.add(com.example.app.filters.PagedFilter.class);
        resources.add(com.example.app.v1.service.AuthenticationEndpoint.class);
        resources.add(com.example.app.v1.service.ConfigEndpoint.class);
        resources.add(com.example.app.v1.service.LogFlushEndpoint.class);
        resources.add(com.example.app.v1.service.TodoEndpoint.class);
    }

}
