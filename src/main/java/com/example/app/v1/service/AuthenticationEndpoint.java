package com.example.app.v1.service;

import com.example.app.Utils;
import com.example.app.beans.Credential;
import com.example.app.beans.RestComponent;
import com.example.app.enums.Role;
import com.example.app.beans.Token;
import com.example.app.beans.User;
import com.example.app.enums.AuthCodes;
import com.example.app.enums.ErrorCodes;
import com.example.app.exception.AppException;
import com.example.app.exception.AuthenticationException;
import com.example.app.session.UserSession;
import java.util.Arrays;
import java.util.Date;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author luca
 */
@Path("/v1/authentication")
@RequestScoped
public class AuthenticationEndpoint extends RestComponent {

    @Inject
    Token token;

    @EJB
    UserSession userSession;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(Credential c) {
        try {
            // Authenticate the user using the credentials provided
            User u = authenticate(c.getUsername(), c.getPassword());
            // Issue a token for the user
            String jwt = token.create(getSessionID(), u);
            // load action
            logAction("LOGIN");
            // Return the token on the response
            return Response.ok(String.format("{ \"token\" : \"%s\" }", jwt)).build();
        } catch (Exception ex) {
            getLogger().error(getLogPrefix() + "login error", ex);
            return AppException.buildResponse(ex);
        }
    }

    private User authenticate(String username, String password) throws Exception, AuthenticationException {
        
        getLogger().info(getLogPrefix() + "Start authentication for: " + username);
        if (Utils.isNullOrEmpty(username) || Utils.isNullOrEmpty(password)) {
            throw new AuthenticationException(ErrorCodes.EA00);
        }

        AuthCodes ac = userSession.authentication(getLogPrefix(), username, password);

        User u = null;

        switch (ac) {
            case SUCCESS:
                u = new User();
                u.setUsername(username);
                u.setUserid(new Date().getTime());
                u.setRoles(Arrays.asList(Role.ADMIN, Role.EDITOR));
                break;
            case NOT_ACTIVE:
                throw new AuthenticationException(ErrorCodes.EA01);
            case NOT_FOUND:
                throw new AuthenticationException(ErrorCodes.EA00);
            case ERROR:
                throw new AuthenticationException(ErrorCodes.UNKNOW);
        }

        getLogger().info(getLogPrefix() + "End authentication " + u);
        return u;
    }

}
