package com.example.app.v1.service;

import com.example.app.beans.RestComponent;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author luca
 */
@Path("/v1/logflush")
@RequestScoped
public class LogFlushEndpoint extends RestComponent {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response logFlush(ClientLog cl) {
        try {
            getLogger().error(getLogPrefix() + cl);
            return Response.ok().build();
        } catch (Exception ex) {
            getLogger().error(getLogPrefix() + "log flush error", ex);
            return Response.ok().build();
        }
    }

    static class ClientLog {

        String frontendUrl;
        String message;
        String type;
        String stack;
        String cause;
        String clientId;
        String clientVersion;

        public String getFrontendUrl() {
            return frontendUrl;
        }

        public void setFrontendUrl(String frontendUrl) {
            this.frontendUrl = frontendUrl;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStack() {
            return stack;
        }

        public void setStack(String stack) {
            this.stack = stack;
        }

        public String getCause() {
            return cause;
        }

        public void setCause(String cause) {
            this.cause = cause;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getClientVersion() {
            return clientVersion;
        }

        public void setClientVersion(String clientVersion) {
            this.clientVersion = clientVersion;
        }

        @Override
        public String toString() {
            return "ClientLog {" + "frontendUrl=" + frontendUrl + ", message=" + message + ", type=" + type + ", stack=" + stack + ", cause=" + cause + ", clientId=" + clientId + ", clientVersion=" + clientVersion + '}';
        }
        
    }
}
