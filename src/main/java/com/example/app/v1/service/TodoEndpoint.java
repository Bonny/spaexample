package com.example.app.v1.service;

import com.example.app.beans.Paged;
import com.example.app.beans.RestComponent;
import com.example.app.beans.Secured;
import com.example.app.beans.User;
import com.example.app.beans.paged.PagedRequest;
import com.example.app.beans.paged.PagedResponse;
import com.example.app.exception.AppException;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author luca
 */
@Path("/todo")
@RequestScoped
public class TodoEndpoint extends RestComponent {
    
    @Paged
    //@Secured
    @Path("/list")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {
        try {
            Thread.sleep(1000L);
            PagedRequest pr = getPagedRequest();
            
            
            PagedResponse<User> res = new PagedResponse<>();
            res.setTotal(80);
            res.setItems(new ArrayList<>());
            pr.getRange().setLimit(2);
            for (int j = 0; j < pr.getRange().getLimit(); j++) {
                long id = (j + 1) * pr.getRange().getOffset();
                User u = new User();
                u.setUserid(id);
                u.setUsername("user-" + id);
                res.getItems().add(u);
            }
            
            return Response.ok(new Gson().toJson(res)).build();
        } catch (Exception ex) {
            getLogger().error(getLogPrefix() + "get config error", ex);
            return AppException.buildResponse(ex);
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response config() {
        try {
            JsonArray a = new JsonArray();
            
            for (int i = 0; i < 10; i++) {
                JsonObject j = new JsonObject();
                j.addProperty("userid", i);
                j.addProperty("name", "user-" + i);
                a.add(j);
            }
            
            JsonObject data = new JsonObject();
            data.addProperty("total", 10);
            data.add("data", a);
            
            return Response.ok(new Gson().toJson(data)).build();
        } catch (Exception ex) {
            getLogger().error(getLogPrefix() + "get config error", ex);
            return AppException.buildResponse(ex);
        }
    }
    
}
