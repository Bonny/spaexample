package com.example.app.v1.service;

import com.example.app.Costants;
import com.example.app.beans.RestComponent;
import com.example.app.beans.Secured;
import com.example.app.exception.AppException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author luca
 */
@Path("/v1/config")
@RequestScoped
public class ConfigEndpoint extends RestComponent {

    @Secured
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response config() {
        try {
            JsonObject todo = new JsonObject();
            
            return Response.ok(new Gson().toJson(todo)).build();
        } catch (Exception ex) {
            getLogger().error(getLogPrefix() + "get config error", ex);
            return AppException.buildResponse(ex);
        }
    }

}
