package com.example.app.filters;

import com.example.app.beans.RestComponent;
import com.example.app.enums.Role;
import com.example.app.beans.Secured;
import com.example.app.enums.ErrorCodes;
import com.example.app.exception.AppException;
import com.example.app.exception.TokenException;
import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author luca
 */
@Secured
@Provider
@Priority(Priorities.AUTHORIZATION)
@RequestScoped
public class AuthorizationFilter extends RestComponent implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // Get the resource class which matches with the requested URL
        // Extract the roles declared by it
        Class<?> resourceClass = getResourceInfo().getResourceClass();
        List<Role> classRoles = extractRoles(resourceClass);

        // Get the resource method which matches with the requested URL
        // Extract the roles declared by it
        Method resourceMethod = getResourceInfo().getResourceMethod();
        List<Role> methodRoles = extractRoles(resourceMethod);

        try {

            List<Role> allowedRoles;
            // Check if the user is allowed to execute the method
            // The method annotations override the class annotations
            if (methodRoles.isEmpty()) {
                allowedRoles = new ArrayList<>(classRoles);
            } else {
                allowedRoles = new ArrayList<>(methodRoles);
            }

            if (!getCurrentUser().hasRoles(allowedRoles)) {
                getLogger().error(getLogPrefix() + String.format("%sThe user not have the roles %s",
                        getCurrentUser().toString(),
                        allowedRoles.toString()));
                throw new TokenException(ErrorCodes.EA02);
            }

        } catch (Exception ex) {
            getLogger().error(getLogPrefix() + "authorization error", ex);
            requestContext.abortWith(AppException.buildResponse(ex));
        }
    }

    // Extract the roles from the annotated element
    private List<Role> extractRoles(AnnotatedElement annotatedElement) {
        if (annotatedElement == null) {
            return new ArrayList<>();
        } else {
            Secured secured = annotatedElement.getAnnotation(Secured.class);
            if (secured == null) {
                return new ArrayList<>();
            } else {
                Role[] allowedRoles = secured.value();
                return Arrays.asList(allowedRoles);
            }
        }
    }

}
