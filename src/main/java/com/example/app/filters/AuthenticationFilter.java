package com.example.app.filters;

import com.example.app.beans.RestComponent;
import com.example.app.beans.Secured;
import com.example.app.beans.Token;
import com.example.app.beans.User;
import com.example.app.exception.AppException;
import java.io.IOException;
import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author luca
 */
@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
@RequestScoped
public class AuthenticationFilter extends RestComponent implements ContainerRequestFilter {

    @Inject
    Token token;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

       // Get the HTTP Authorization header from the request
        final String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        // Check if the HTTP Authorization header is present and formatted correctly
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            getLogger().warn(getLogPrefix() + "Authorization header must be provided");
            throw new NotAuthorizedException("Authorization header must be provided");
        }

        // Extract the token from the HTTP Authorization header
        String jwt = authorizationHeader.substring("Bearer".length()).trim();

        try {
            User u = token.parse(getSessionID(), jwt);
            getCurrentUser().setUser(u);
        } catch (Exception ex) {
            getLogger().error(getLogPrefix() + "authtentication error [" + jwt + "]", ex);
            requestContext.abortWith(AppException.buildResponse(ex));
        }
    }

}
