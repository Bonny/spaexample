package com.example.app.filters;

import com.example.app.beans.RestComponent;
import java.io.IOException;
import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author luca
 */
@Provider
@Priority(Priorities.USER)
@RequestScoped
public class LogFilter extends RestComponent implements ContainerRequestFilter, ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext crc) throws IOException {
        try {
            //id generato dal client (per log lato client)
            String _rID = getRequest().getParameter("_rID");
            getCurrentUser().setStartTime(System.currentTimeMillis());
            final String resource = String.format("[_rId=%s][%s] %s", 
                    _rID,
                    crc.getMethod(),
                    crc.getUriInfo().getAbsolutePath());
            getLogger().info(String.format("%sRequest %s", getLogPrefix(), resource));
        } catch (Exception e) {
            getLogger().error(getLogPrefix() + "Unknow error", e);
        }
    }

    @Override
    public void filter(ContainerRequestContext crc, ContainerResponseContext crc1) throws IOException {
        final String resource = String.format("[%s] %s", crc.getMethod(),
                crc.getUriInfo().getAbsolutePath());
        getLogger().info(String.format("%sResponse to %s request in %dms",
                getLogPrefix(),
                resource,
                System.currentTimeMillis() - getCurrentUser().getStartTime()
        ));
    }

}
