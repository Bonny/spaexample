package com.example.app.filters;

import com.example.app.beans.Paged;
import com.example.app.beans.RestComponent;
import com.example.app.beans.paged.Field;
import com.example.app.beans.paged.GridFilter;
import com.example.app.beans.paged.GridRange;
import com.example.app.beans.paged.GridSorter;
import com.example.app.beans.paged.PagedRequest;
import com.example.app.exception.AppException;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author luca
 */
@Paged
@Provider
@Priority(Priorities.HEADER_DECORATOR)
@RequestScoped
public class PagedFilter extends RestComponent implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        try {
            MultivaluedMap<String, String> qp = requestContext.getUriInfo().getQueryParameters();
            String paged = qp.getFirst("paged");
            getLogger().debug("paged=" + paged);
            PagedRequest pr = parse(paged);
            setPagedRequest(pr);
        } catch (Exception ex) {
            getLogger().error(getLogPrefix() + "paged error", ex);
            requestContext.abortWith(AppException.buildResponse(ex));
        } finally {
            PagedRequest pr = getPagedRequest();
            getLogger().debug(getLogPrefix() + pr);
        }
    }

    private GridRange parseRange(JsonObject json) {
        GridRange gr = new GridRange();
        try {
            JsonObject range = json.getAsJsonObject("range");
            gr.setLimit(range.get("limit").getAsInt());
            gr.setOffset(range.get("offset").getAsInt());
        } catch (Exception ex) {
            getLogger().warn(getLogPrefix() + "Parse [Range] error " + ex);
        }
        return gr;
    }

    private GridSorter parseSorter(JsonObject json) {
        GridSorter gs = new GridSorter();
        try {
            JsonObject sorter = json.getAsJsonObject("sorter");
            gs.setProperty(sorter.get("property").getAsString());
            gs.setDirection(sorter.get("direction").getAsString());
        } catch (Exception ex) {
            getLogger().warn(getLogPrefix() + "Parse [Sorter] error " + ex);
        }
        return gs;
    }

    private GridFilter parseFilter(JsonObject json) {
        GridFilter gf = new GridFilter();
        try {
            JsonObject filter = json.getAsJsonObject("filter");
            if (filter.has("relation")) {
                gf.setRelation(filter.get("relation").getAsString());
            }
            JsonArray fields = filter.getAsJsonArray("fields");
            fields.forEach(i -> {
                JsonObject o = new Gson().fromJson(i, JsonObject.class);
                Field f = new Field();
                f.setOperator(GridFilter.Operator.get(o.get("operator").getAsString()));
                f.setType(GridFilter.Type.get(o.get("type").getAsString()));
                f.setDataIndex(o.get("dataIndex").getAsString());
                if (o.get("value").isJsonObject()) {
                    JsonObject v = o.get("value").getAsJsonObject();
                    f.setValue(v.get("from").getAsString());
                    f.setValue2(v.get("to").getAsString());
                } else if (o.get("value").isJsonPrimitive()) {
                    f.setValue(o.get("value").getAsString());
                }
                gf.getFields().add(f);
            });
        } catch (Exception ex) {
            getLogger().warn(getLogPrefix() + "Parse [Filter] error " + ex);
        }
        return gf;
    }

    private PagedRequest parse(String paged) {
        final PagedRequest pr = new PagedRequest();
        try {
            Gson gson = new Gson();
            JsonObject json = gson.fromJson(paged, JsonObject.class);
            pr.setRange(parseRange(json));
            pr.setSorter(parseSorter(json));
            pr.setFilter(parseFilter(json));
        } catch (JsonSyntaxException ex) {
            getLogger().warn(getLogPrefix() + "Parse error " + ex.getMessage());
        }
        return pr;
    }
}
