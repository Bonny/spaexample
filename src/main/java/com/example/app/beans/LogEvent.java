package com.example.app.beans;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author luca
 */
public class LogEvent {

    User user;
    String resource;
    String sessUID;
    Date ts;
    Map<String, String> info;

    public LogEvent() {
        ts = new Date();
    }

    public Map<String, String> getInfo() {
        if (info == null) {
            info = new HashMap<>();
        }
        return info;
    }

    public void addInfo(String key, String value) {
        getInfo().put(key, value);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSessUID() {
        return sessUID;
    }

    public void setSessUID(String sessUID) {
        this.sessUID = sessUID;
    }

    public Date getTs() {
        return ts;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    @Override
    public String toString() {
        return "LogEvent{" + user + ", resource=" + resource + ", sessUID=" + sessUID + ", ts=" + ts + "}";
    }

}
