package com.example.app.beans.paged;

import com.example.app.Utils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Per filtrare i dati inviati dal client javascript
 *
 * @author lbonaldo
 *
 */
public class GridFilter implements Serializable {

    private static final long serialVersionUID = 12L;

    private String relation = "AND";

    private List<Field> fields;

    public static enum Type {
        UNDEFINED, DATE, NUMBER, TEXT, BOOL;

        public static Type get(String name) {
            Type t = UNDEFINED;
            if (name == null || name.length() == 0) {
                return t;
            }
            int i = 0;
            boolean found = false;
            while (i < Type.values().length && !found) {
                if (name.equalsIgnoreCase(Type.values()[i].name())) {
                    t = Type.values()[i];
                    found = true;
                } else {
                    i++;
                }
            }
            return t;
        }
    }

    public static enum Operator {
        UNDEFINED(""),
        LIKE(" like "), NOT_LIKE(" not like "), IN(" in "), NOT_IN(" not in "), EQUALS(" = "), NOT_EQUALS(" <> "), GREATER(" > "), GREATER_AND_EQUALS(" >= "), LESS(" < "), LESS_AND_EQUALS(" <= "), BETWEEN(
                " between "), NOT_BETWEEN(" not between ");

        private String opt = "";

        private Operator(String opt) {
            this.opt = opt;
        }

        public String get() {
            return this.opt;
        }

        public static Operator get(String name) {
            Operator o = UNDEFINED;
            if (name == null || name.length() == 0) {
                return o;
            }
            int i = 0;
            boolean found = false;
            while (i < Operator.values().length && !found) {
                if (name.equalsIgnoreCase(Operator.values()[i].name())) {
                    o = Operator.values()[i];
                    found = true;
                } else {
                    i++;
                }
            }
            return o;
        }
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public boolean isEmpty() {
        return getFields().isEmpty();
    }

    public List<Field> getFields() {
        if (fields == null) {
            fields = new ArrayList<>();
        }
        return fields;
    }

    @Override
    public String toString() {
        return "GridFilter [relation=" + relation + ", fields=" + Utils.toString(getFields()) + "]";
    }

}
