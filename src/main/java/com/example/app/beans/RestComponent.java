package com.example.app.beans;

import com.example.app.beans.paged.PagedRequest;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import org.apache.log4j.Logger;

/**
 *
 * @author luca
 */
public abstract class RestComponent {

    @Inject
    Logger logger;

    @Context
    ResourceInfo resourceInfo;

    @Inject
    CurrentUser currentUser;

    @Context
    HttpServletRequest request;

    @Inject
    Event<LogEvent> logEvents;

    protected PagedRequest getPagedRequest() {
        PagedRequest pr = (PagedRequest) getRequest().getAttribute("paged");
        return pr != null ? pr : new PagedRequest();
    }

    protected void setPagedRequest(PagedRequest pr) {
        getRequest().setAttribute("paged", pr);
    }

    protected void logAction(String resource) {
        final LogEvent evt = new LogEvent();
        evt.setResource(resource);
        evt.setSessUID(getSessionID());
        evt.setUser(getCurrentUser().getUser());
        evt.addInfo("host", getRequest().getRemoteHost());
        evt.addInfo("User-Agent", getRequest().getHeader("User-Agent"));
        logEvents.fire(evt);
    }

    public String getLogPrefix() {
        StringBuilder sb = new StringBuilder();
        sb.append("[sUID=").append(getSessionID()).append("] ");
        sb.append("[rId=").append(currentUser.getRequestId()).append("] ");
        if (currentUser.isPresent()) {
            sb.append("[user=").append(currentUser.getUser().getUsername()).append("] ");
        }
        return sb.toString();
    }

    protected Logger getLogger() {
        return logger;
    }

    protected ResourceInfo getResourceInfo() {
        return resourceInfo;
    }

    protected CurrentUser getCurrentUser() {
        return currentUser;
    }

    protected HttpServletRequest getRequest() {
        return request;
    }

    protected String getSessionID() {
        return getRequest().getSession().getId();
    }
}
