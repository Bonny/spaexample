package com.example.app.beans;

import com.example.app.Utils;
import com.example.app.enums.AppProperties;
import com.example.app.enums.ErrorCodes;
import com.example.app.exception.TokenException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.crypto.MacProvider;
import java.security.Key;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.apache.log4j.Logger;

/**
 *
 * @author luca
 */
@Singleton
public class Token {

    @Inject
    AppSingleton app;

    @Inject
    Logger LOGGER;

    private final SignatureAlgorithm SAlg = SignatureAlgorithm.HS512;
    private final Key key = MacProvider.generateKey();

//    @PostConstruct
//    void postConstruct() {
//        LOGGER.debug("Token has benn inizialized");
//    }
//
//    @PreDestroy
//    void preDestroy() {
//        LOGGER.debug("Token has benn destroyed");
//    }

    public User parse(final String sessUID, final String jwt) throws TokenException {
        try {

            Jws<Claims> claims = Jwts.parser().setSigningKey(key).parseClaimsJws(jwt);
            Claims body = claims.getBody();
            String user = body.get("user", String.class);

            String id = claims.getBody().getId();

            if (!Utils.isNullOrEmpty(id) && !id.equals(sessUID)) {
                LOGGER.warn(String.format("Session id of client is different of token current=%s token=%s", sessUID, id));
            }

            return new GsonBuilder().create().fromJson(user, User.class);

        } catch (ExpiredJwtException e) {
            // if the specified JWT is a Claims JWT and the Claims has an expiration time
            // before the time this method is invoked.
            throw new TokenException(ErrorCodes.ET01, e);
        } catch (IllegalArgumentException e) {
            // if the specified string is {@code null} or empty or only whitespace, or if the
            // {@code handler} is {@code null}.
            throw new TokenException(ErrorCodes.ET02, e);
        } catch (MalformedJwtException e) {
            // if the specified JWT was incorrectly constructed (and therefore invalid).
            // Invalid JWTs should not be trusted and should be discarded.
            throw new TokenException(ErrorCodes.ET03, e);
        } catch (SignatureException e) {
            // if a JWS signature was discovered, but could not be verified.  JWTs that fail
            // signature validation should not be trusted and should be discarded
            throw new TokenException(ErrorCodes.ET04, e);
        } catch (Exception e) {
            // unknow error       
            throw new TokenException(e);
        }
    }

    public String create(final String sessUID, final User user) throws TokenException {
        Long m = app.getLongProperty(AppProperties.TOKEN_TS_EXPIRATION);
        m = TimeUnit.MINUTES.toMillis(m);
        m = 60000L;
        m *= 20;
        return create(sessUID, user, m);
    }

    private String create(final String sessUID, final User user, final long ttlMillis) throws TokenException {
        try {

            final long nowMillis = System.currentTimeMillis();
            final Date now = new Date(nowMillis);

            //Let's set the JWT Claims
            JwtBuilder builder = Jwts.builder();

            //if it has been specified, let's add the expiration
            if (ttlMillis >= 0) {
                long expMillis = nowMillis + ttlMillis;
                Date exp = new Date(expMillis);
                LOGGER.debug("Exp: " + exp);
                builder.setExpiration(exp);
                //builder.claim("expired", exp.getTime());
            }

            builder.setId(sessUID);
            builder.setIssuedAt(now);
            builder.claim("user", new Gson().toJson(user));

            builder.signWith(SAlg, key);

            return builder.compact();

        } catch (Exception ex) {
            throw new TokenException(ErrorCodes.ET00, ex);
        }
    }

}
