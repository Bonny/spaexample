package com.example.app.beans;

import com.example.app.enums.Role;
import com.example.app.Utils;
import com.example.app.filters.AuthenticationFilter;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import org.apache.log4j.Logger;

/**
 *
 * @author luca
 */
@RequestScoped
public class CurrentUser {

    /**
     * User info, set in {@link AuthenticationFilter}
     */
    private User user;

    /**
     * Startup time of request
     */
    private long startTime = 0;

    /**
     * Random value to try to distinguish simultaneous calls from the same
     * client or with the same session id
     */
    private long requestId;

    @Inject
    Logger LOGGER;

    @PostConstruct
    void postConstruct() {
        //requestId = System.currentTimeMillis();
        java.util.UUID uuid = java.util.UUID.randomUUID();
        requestId = Math.abs(uuid.hashCode());
    }

    public long getRequestId() {
        return requestId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isPresent() {
        return user != null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (isPresent()) {
            sb.append("[").append(getUser().getUsername()).append("] ");
        }
        return sb.toString();
    }

    /**
     * This method check user's roles
     *
     * @param roles Roles of resourse requested
     * @return true if user contains all roles
     */
    public boolean hasRoles(List<Role> roles) {
        if (Utils.isNullOrEmpty(roles)) {
            return true;
        }
        if (Utils.isNullOrEmpty(getUser().getRoles())) {
            return false;
        }
        boolean ret = true;
        for (Role r : roles) {
            ret = ret && getUser().getRoles().contains(r);
        }
        return ret;
    }
}
