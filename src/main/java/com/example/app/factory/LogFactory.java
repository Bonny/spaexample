package com.example.app.factory;

import com.example.app.beans.LogEvent;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Named;
import javax.inject.Singleton;
import org.apache.log4j.Logger;

/**
 *
 * @author luca
 */
@Named
@Singleton
public class LogFactory {

    @Produces
    Logger createLogger(InjectionPoint injectionPoint) {
        return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    }
    
    void observeLogEvent(@Observes LogEvent logEvent) {
        final Logger LOGGER = Logger.getLogger(LogFactory.class);
        LOGGER.info("Observe: " + logEvent);
    }
}
