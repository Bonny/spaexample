package com.example.app.exception;

import com.example.app.enums.ErrorCodes;

/**
 *
 * @author luca
 */
public class AuthenticationException extends AppException {

    public AuthenticationException() {
        super();
    }

    public AuthenticationException(ErrorCodes err) {
        super(err);
    }

    public AuthenticationException(ErrorCodes err, Throwable cause) {
        super(err, cause);
    }

    public AuthenticationException(Throwable cause) {
        super(cause);
    }

}
