package com.example.app.exception;

import com.example.app.enums.ErrorCodes;

/**
 *
 * @author luca
 */
public class TokenException extends AppException {
    
    public TokenException() {
        super();
    }

    public TokenException(ErrorCodes err) {
        super(err);
    }

    public TokenException(ErrorCodes err, Throwable cause) {
        super(err, cause);
    }

    public TokenException(Throwable cause) {
        super(cause);
    }

}
