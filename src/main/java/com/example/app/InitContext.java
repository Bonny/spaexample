package com.example.app;

import com.example.app.beans.AppSingleton;
import java.io.File;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.servlet.annotation.WebListener;

@WebListener
public class InitContext implements ServletContextListener {

    @Inject
    Logger LOGGER;

    @Inject
    AppSingleton app;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Start " + Costants.DISPLAY_NAME + " ...");
        try {
            app.init();
            String confpath = System.getProperty("APP_CONF_PATH") + File.separator + "qdesk.properties";
            if (!new java.io.File(confpath).exists()) {
                throw new RuntimeException("The file not exists: " + confpath);
            }
            app.load(confpath);
            LOGGER.info(String.format("%s context initialized successful", Costants.DISPLAY_NAME));
        } catch (Exception ex) {
            LOGGER.error(String.format("%s context initialized failed", Costants.DISPLAY_NAME), ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Stop " + Costants.DISPLAY_NAME);
        LOGGER.info(String.format("%s context destroyed successful", Costants.DISPLAY_NAME));
    }
}
