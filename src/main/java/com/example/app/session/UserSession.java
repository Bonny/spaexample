package com.example.app.session;

import com.example.app.Utils;
import com.example.app.enums.AuthCodes;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.log4j.Logger;

/**
 *
 * @author luca
 */
@Stateless
@LocalBean
public class UserSession {

    final Logger LOGGER = Logger.getLogger(UserSession.class);

    
    
    public AuthCodes authentication(final String logPrefix, final String username, final String password) {
        try {

            LOGGER.info(logPrefix + "authentication: " + username);

            if (Utils.isNullOrEmpty(username) || Utils.isNullOrEmpty(password)) {
                throw new Exception("usernam or password has null or empty");
            }

            if (username.equals("admin")) {
                return AuthCodes.SUCCESS;
            }

            if (username.equals("admin2")) {
                return AuthCodes.NOT_ACTIVE;
            }

            return AuthCodes.NOT_FOUND;

        } catch (Exception ex) {
            LOGGER.error(logPrefix + ex.getMessage());
            return AuthCodes.ERROR;
        }
    }
}
