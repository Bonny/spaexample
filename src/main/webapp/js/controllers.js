(function () {
    'use strict';

    angular.module('app').controller('HomeCtrl', ['$scope', '$localStorage', '$auth', '$log',
        function ($scope, $localStorage, $auth, $log) {
            $log.debug("HomeCtrl.load");
            $scope.token = $localStorage.token;
            $scope.tokenClaims = $auth.getTokenClaims();
        }]);

    angular.module('app').controller('TopMenuCtrl', ['$scope', '$auth', '$state', '$logger', 'info',
        function ($scope, $auth, $state, $logger, info) {
            $logger.debug("TopMenuCtrl.load");
            $scope.info = info;
            $scope.user = $auth.getUser();
            $scope.logout = function () {
                $auth.logout(function () {
                    $logger.debug("TopMenuCtrl go -> index");
                    $state.go("index");
                });
            };
            $scope.showInfo = function () {
                $("#appInfoModal").modal('show');
            };
        }]);

    angular.module('app').controller('MainCtrl', ['$scope', '$auth', '$state', '$logger', '$config', '$http', 'urls', '$toast',
        function ($scope, $auth, $state, $logger, $config, $http, urls, $toast) {
            $logger.info("MainCtrl.load");
            $scope.init = function () {
                $toast.success("ciaooooooooo");
                $toast.danger("ciaooooooooo");
                $toast.warning("ciaooooooooo");
                if ($auth.isAuth()) {
                    $logger.info("MainCtrl.init isAuth");
                    // se autenticato reload config
                    $config.init(function () {
                        $logger.debug("MainCtrl go -> home");
                        $state.go("home");
                    });
                } else {
                    $logger.info("MainCtrl.init !isAuth");
                    $logger.debug("MainCtrl go -> signin");
                    $state.go("signin");
                }
            };
            $scope.$on("$TokenExpired", function () {
                $logger.info("MainCtrl.$TokenExpired");
                if ($config.isInit()) {
                    $("#expiredModal").modal({keyboard: false});
                } else {
                    $logger.debug("MainCtrl.$TokenExpired go -> signin");
                    $state.go("signin");
                }
            });
            //non in $logger perchè crea dipendenza ciclica con $http > $httpInterceptor
            $scope.$on("$LogFlush", function (data) {
                $http.post(urls.logflush, data);
            });
            $scope.init();
        }]);

    angular.module('app').controller('TestCtrl', ['$scope', '$logger', 'urls',
        function ($scope, $logger, urls) {

            $scope.datetime = new Date().getTime();

            $scope.range = {
                from: 0,
                to: 0,
                invalid: false
            };

            var columns = [];

            columns.push({
                tabIndex: 1,
                header: "id",
                dataIndex: "userid",
                flex: 1,
                sortable: true,
                filterable: new FilterableNumber()
            });

            columns.push({
                tabIndex: 2,
                header: "Username",
                dataIndex: "username",
                flex: 1,
                sortable: true,
                filterable: new FilterableText()
            });

            columns.push({
                tabIndex: 3,
                header: "Date",
                dataIndex: "ts",
                flex: 1,
                sortable: true,
                filterable: new FilterableDate()
            });

            var actions = [];

            actions.push({
                title: "edit",
                multiple: false,
                icon: 'fa fa-edit',
                callbackFn: function (rows) {
                    $logger.info(rows || []);
                }
            });
            actions.push({
                title: "view",
                icon: 'fa fa-file-alt',
                multiple: false,
                callbackFn: function (rows) {
                    $logger.info(rows || []);
                }
            });
            actions.push({
                title: "delete",
                icon: 'fa fa-trash-alt',
                multiple: true,
                callbackFn: function (rows) {
                    $logger.info(rows || []);
                }
            });

            var toolbar = [{
                    title: "Upload",
                    icon: 'fa fa-upload',
                    callbackFn: function (rows) {
                        $logger.info("upload");
                    }
                }];


            $scope.gridConfig = {
                url: urls.baseUrl + "/todo/list",
                columns: columns,
                striped: false,
                hover: true,
                selectable: true,
                displayBtnLabel: false,
                actions: actions,
                toolbar: toolbar
            };
        }]);
})();