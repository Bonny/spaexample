(function () {
    'use strict';

    var app = angular.module('app', [
        //ext
        'ngStorage',
        'angular-loading-bar',
        'ui.router',
        'ui.router.breadcrumbs',
        'ngAnimate',
        'gettext',
        //local
        'ngCommon',
        'ngInput',
        'ngGrid',
        'ngAuth'
    ]);

    var BASE = "./rest";

    app.constant('info', {
        name: "SPAExample",
        version: "1.0.0",
        relDate: "01/04/2018"
    });

    app.constant('urls', {//mandatory
        baseUrl: BASE,
        auth: BASE + '/v1/authentication',
        config: BASE + '/v1/config',
        logflush: BASE + '/v1/logflush'
    });

    app.run(['$rootScope', '$state', '$stateParams', 'gettextCatalog',
        function ($rootScope, $state, $stateParams, gettextCatalog) {

            gettextCatalog.currentLanguage = 'it';

            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;

            $rootScope.$on('$stateChangeStart',
                    function (event, toState, toParams, fromState, fromParams, options) {
                        //event.preventDefault();                    
                        //var from = angular.toJson(fromState);
                        //var to = angular.toJson(toState);
                        //$logger.info("$stateChangeStart: fromState=" + from + ", toState=" + to);
                    });

            $rootScope.$on('$stateNotFound',
                    function (event, unfoundState, fromState, fromParams) {
                        //var from = angular.toJson(fromState);
                        //var unfound = angular.toJson(unfoundState);
                        //$logger.info("$stateNotFound: fromState=" + from + ", unfoundState=" + unfound);
                    });

            $rootScope.$on('$stateChangeSuccess',
                    function (event, toState, toParams, fromState, fromParams) {
                        //var from = angular.toJson(fromState);
                        //var to = angular.toJson(toState);
                        //$logger.info("$stateChangeSuccess: fromState=" + from + ", toState=" + to);
                    });

            $rootScope.$on('$viewContentLoading',
                    function (event, viewConfig) {
                       // var view = angular.toJson(viewConfig);
                        //$logger.info("$viewContentLoading: viewConfig=" + view);
                    });

            $rootScope.$on('$viewContentLoaded',
                    function (event) {
                        //$logger.info("$viewContentLoaded");
                    });
        }]);

    app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 'cfpLoadingBarProvider',
        function ($stateProvider, $urlRouterProvider, $httpProvider, cfpLoadingBarProvider) {

            ////// Loading bar config //////

            //cfpLoadingBarProvider.includeSpinner = false;
            //cfpLoadingBarProvider.includeBar = false;
            //cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
            //cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Custom Loading Message...</div>';

            ////// token interceptor config //////

            $httpProvider.interceptors.push('$httpInterceptor');// see ngAuth

            ////// ui route config //////

            $urlRouterProvider.otherwise('/index');

            function check_auth($state, $auth) {
                if (!$auth.isAuth()) {
                    $state.go('signin');
                }
            }

            $stateProvider
                    .state('logs', {
                        url: '/logs',
                        controller: ['$scope', '$logger', function ($scope, $logger) {
                                $scope.getContent = function () {
                                    return $logger.getAll();
                                };
                            }],
                        template: '<button ui-sref="index" type="button" class="btn btn-link mx-auto mb-3">' +
                                '<i class="fa fa-long-arrow-alt-left"></i> <span translate>back</span>' +
                                '</button>' +
                                '<pre ng-repeat="i in getContent() track by $index" class="m-0">{{i}}</pre>'
                    })
                    .state('signin', {
                        url: '/signin',
                        template: '<div class="container-fluid" id="signin-container">' +
                                '       <div class="row">' +
                                '         <div class="col-md-12">' +
                                '            <signin></signin>' +
                                '         </div>' +
                                '       </div>' +
                                '     </div>'
                    })
                    .state('home', {
                        url: '/home',
                        controller: 'HomeCtrl',
                        templateUrl: 'partials/home.html',
                        onEnter: check_auth,
                        breadcrumb: {
                            text: "Home",
                            stateName: 'home'
                        }
                    })
                    .state('index', {
                        url: '/index',
                        cotroller: "MainCtrl",
                        template: '<div class="card mx-auto" ng-init="init()" style="width: 20rem;">' +
                                '    <div class="card-body">' +
                                '      <loading size="large" text="true"></loading>' +
                                '    </div>' +
                                '  </div>'
                    });

        }
    ]);

})();