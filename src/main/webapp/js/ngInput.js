/**
 * Auth module
 */


/**
 * ####### DIRECTIVES #######
 */

(function () {
    'use strict';

    var autocomplete = ["$parse", "$http", "$sce", "$timeout", function ($parse, $http, $sce, $timeout) {
            return {
                restrict: 'EA',
                scope: {
                    "id": "@id",
                    "placeholder": "@placeholder",
                    "selectedObject": "=selectedobject",
                    "url": "@url",
                    "dataField": "@datafield",
                    "titleField": "@titlefield",
                    "descriptionField": "@descriptionfield",
                    "imageField": "@imagefield",
                    "imageUri": "@imageuri",
                    "inputClass": "@inputclass",
                    "userPause": "@pause",
                    "localData": "=localdata",
                    "searchFields": "@searchfields",
                    "minLengthUser": "@minlength",
                    "matchClass": "@matchclass"
                },
                templateUrl: './partials/autocomplete.html',
                link: function (scope, elem, attrs) {

                    scope.noelementfoundLabel = "not-element-found";
                    scope.searchingLabel = "search";
                    scope.lastSearchTerm = null;
                    scope.currentIndex = null;
                    scope.justChanged = false;
                    scope.searchTimer = null;
                    scope.hideTimer = null;
                    scope.searching = false;
                    scope.pause = 500;
                    scope.minLength = 3;
                    scope.searchStr = null;

                    if (scope.minLengthUser && scope.minLengthUser != "") {
                        scope.minLength = scope.minLengthUser;
                    }

                    if (scope.userPause) {
                        scope.pause = scope.userPause;
                    }

                    var isNewSearchNeeded = function (newTerm, oldTerm) {
                        return newTerm.length >= scope.minLength && newTerm != oldTerm
                    };

                    scope.processResults = function (responseData, str) {
                        if (responseData && responseData.length > 0) {
                            scope.results = [];

                            var titleFields = [];
                            if (scope.titleField && scope.titleField != "") {
                                titleFields = scope.titleField.split(",");
                            }

                            for (var i = 0; i < responseData.length; i++) {
                                // Get title variables
                                var titleCode = [];

                                for (var t = 0; t < titleFields.length; t++) {
                                    titleCode.push(responseData[i][titleFields[t]]);
                                }

                                var description = "";
                                if (scope.descriptionField) {
                                    description = responseData[i][scope.descriptionField];
                                }

                                var imageUri = "";
                                if (scope.imageUri) {
                                    imageUri = scope.imageUri;
                                }

                                var image = "";
                                if (scope.imageField) {
                                    image = imageUri + responseData[i][scope.imageField];
                                }

                                var text = titleCode.join(' ');
                                if (scope.matchClass) {
                                    var re = new RegExp(str, 'i');
                                    var strPart = text.match(re)[0];
                                    text = $sce.trustAsHtml(text.replace(re, '<span class="' + scope.matchClass + '">' + strPart + '</span>'));
                                }

                                var resultRow = {
                                    title: text,
                                    description: description,
                                    image: image,
                                    originalObject: responseData[i]
                                };

                                scope.results[scope.results.length] = resultRow;
                            }


                        } else {
                            scope.results = [];
                        }
                    };

                    scope.searchTimerComplete = function (str) {
                        // Begin the search

                        if (str.length >= scope.minLength) {
                            if (scope.localData) {
                                var searchFields = scope.searchFields.split(",");

                                var matches = [];

                                for (var i = 0; i < scope.localData.length; i++) {
                                    var match = false;

                                    for (var s = 0; s < searchFields.length; s++) {
                                        match = match || (typeof scope.localData[i][searchFields[s]] === 'string' && typeof str === 'string' && scope.localData[i][searchFields[s]].toLowerCase().indexOf(str.toLowerCase()) >= 0);
                                    }

                                    if (match) {
                                        matches[matches.length] = scope.localData[i];
                                    }
                                }

                                scope.searching = false;
                                scope.processResults(matches, str);

                            } else {
                                $http.get(scope.url + str, {}).
                                        success(function (responseData, status, headers, config) {
                                            scope.searching = false;
                                            scope.processResults(((scope.dataField) ? responseData[scope.dataField] : responseData), str);
                                        }).
                                        error(function (data, status, headers, config) {
                                            console.log("error");
                                        });
                            }
                        }
                    };

                    scope.hideResults = function () {
                        scope.hideTimer = $timeout(function () {
                            scope.showDropdown = false;
                        }, scope.pause);
                    };

                    scope.resetHideResults = function () {
                        if (scope.hideTimer) {
                            $timeout.cancel(scope.hideTimer);
                        }
                        ;
                    };

                    scope.hoverRow = function (index) {
                        scope.currentIndex = index;
                    };

                    scope.keyPressed = function (event) {
                        if (!(event.which == 38 || event.which == 40 || event.which == 13)) {
                            if (!scope.searchStr || scope.searchStr == "") {
                                scope.showDropdown = false;
                                scope.lastSearchTerm = null
                            } else if (isNewSearchNeeded(scope.searchStr, scope.lastSearchTerm)) {
                                scope.lastSearchTerm = scope.searchStr
                                scope.showDropdown = true;
                                scope.currentIndex = -1;
                                scope.results = [];

                                if (scope.searchTimer) {
                                    $timeout.cancel(scope.searchTimer);
                                }

                                scope.searching = true;

                                scope.searchTimer = $timeout(function () {
                                    scope.searchTimerComplete(scope.searchStr);
                                }, scope.pause);
                            }
                        } else {
                            event.preventDefault();
                        }
                    };

                    scope.selectResult = function (result) {
                        if (scope.matchClass) {
                            result.title = result.title.toString().replace(/(<([^>]+)>)/ig, '');
                        }
                        scope.searchStr = scope.lastSearchTerm = result.title;
                        scope.selectedObject = result;
                        scope.showDropdown = false;
                        scope.results = [];
                        //scope.$apply();
                    };

                    var inputField = elem.find('input');

                    inputField.on('keyup', scope.keyPressed);

                    elem.on("keyup", function (event) {
                        if (event.which === 40) {
                            if (scope.results && (scope.currentIndex + 1) < scope.results.length) {
                                scope.currentIndex++;
                                scope.$apply();
                                event.preventDefault;
                                event.stopPropagation();
                            }

                            scope.$apply();
                        } else if (event.which == 38) {
                            if (scope.currentIndex >= 1) {
                                scope.currentIndex--;
                                scope.$apply();
                                event.preventDefault;
                                event.stopPropagation();
                            }

                        } else if (event.which == 13) {
                            if (scope.results && scope.currentIndex >= 0 && scope.currentIndex < scope.results.length) {
                                scope.selectResult(scope.results[scope.currentIndex]);
                                scope.$apply();
                                event.preventDefault;
                                event.stopPropagation();
                            } else {
                                scope.results = [];
                                scope.$apply();
                                event.preventDefault;
                                event.stopPropagation();
                            }

                        } else if (event.which == 27) {
                            scope.results = [];
                            scope.showDropdown = false;
                            scope.$apply();
                        } else if (event.which == 8) {
                            scope.selectedObject = null;
                            scope.$apply();
                        }
                    });

                }
            };
        }];


    var datePicker = ['$utils', '$logger', function ($utils, $logger) {
            return {
                restrict: 'E',
                transclude: true,
                template: '<small>{{label|translate}}</small>' +
                        '<div class="d-flex flex-row">' +
                        '    <select ng-model="day" ng-change="rebind()" ng-class="{\'invalid-input\':isInvalid()}" style="width: 8rem;" class="form-control form-control-sm">' +
                        '      <option ng-repeat="d in days" value="{{d.index}}">{{ "d_" + d.day |translate }} {{d.index}}</option>' +
                        '    </select>' +
                        '    &sol;' +
                        '    <select ng-model="month" ng-change="change()" ng-class="{\'invalid-input\':isInvalid()}" style="width: 8rem;" class="form-control form-control-sm">' +
                        '      <option ng-repeat="m in months" ng-value="m">{{ "m_" + m |translate }}</option>' +
                        '    </select>' +
                        '    &sol;' +
                        '    <select ng-model="year" ng-change="change()" ng-class="{\'invalid-input\':isInvalid()}" style="width: 8rem;" class="form-control form-control-sm">' +
                        '      <option ng-repeat="y in years" ng-value="y">{{y}}</option>' +
                        '    </select>' +
                        '</div>',
                scope: {
                    value: '=',
                    invalid: '=',
                    label: '@'
                },
                link: function (scope, element, attrs) {

                    scope.reset = function () {
                        scope.day = scope.month = scope.year = -1;
                    };

                    if (scope.value) {
                        var date = moment(scope.value);
                        scope.day = date.get("date");
                        scope.month = date.get("month");
                        scope.year = date.get("year");
                    } else {
                        scope.reset();
                    }

                    scope.isInvalid = function () {
                        return $utils.isBool(scope.invalid);
                    };

                    scope.days = [];
                    scope.years = [];
                    scope.months = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

                    for (var y = 2010; y <= (new Date().getFullYear()); y++) {
                        scope.years.push(y);
                    }

                    scope.reconfigure = function () {
                        var start = moment({year: scope.year, month: scope.month, date: scope.day});
                        scope.days = [];
                        for (var i = 0; i < start.daysInMonth(); i++) {
                            var index = i + 1;
                            var date = moment({year: scope.year, month: scope.month, date: index});
                            var day = date.day();
                            scope.days.push({
                                index: index,
                                day: day
                            });
                        }
                    };

                    scope.change = function () {
                        scope.day = 1;
                        scope.reconfigure();
                        scope.rebind();
                    };

                    scope.rebind = function () {
                        var date = {year: scope.year, month: scope.month, date: scope.day};
                        var m = moment(date);
                        scope.value = m.valueOf();
                        $logger.info("rebind -> value=" + scope.value);
                    };

                    if (scope.value)
                        scope.reconfigure();

                    scope.$on("$ResetDatePicker", function () {
                        $logger.debug("$ResetDatePicker");
                        scope.reset();
                        scope.reconfigure();
                    });
                }
            };
        }];

    var datePickerRange = ['$logger', '$rootScope', function ($logger, $rootScope) {
            return {
                restrict: 'E',
                transclude: true,
                scope: {
                    value: '='
                },
                template: '<div class="d-flex flex-column">' +
                        '   <date-picker value="value.from" label="from" invalid="value.invalid"></date-picker>' +
                        '   <date-picker value="value.to" label="to" invalid="value.invalid"></date-picker>' +
                        '   <button ng-click="reset()" style="width:0rem" type="button" class="btn btn-sm btn-link" >' +
                        '     <!--i class="fa fa-undo"></i--> Reset' +
                        '   </button>' +
                        '</div>',
                link: function (scope, element, attrs) {

                    var validate = function () {
                        scope.value.invalid = scope.value.from > scope.value.to;
                        $logger.info("value changed " + JSON.stringify(scope.value));
                    };

                    scope.$watch('value.from', validate);
                    scope.$watch('value.to', validate);

                    scope.reset = function () {
                        scope.value.invalid = false;
                        scope.value.from = scope.value.to = 0;
                        $rootScope.$broadcast('$ResetDatePicker');
                    };
                }
            };
        }];

    angular.module("ngInput.directives", [])
            .directive("datePicker", datePicker)
            .directive("datePickerRange", datePickerRange)
            .directive("autocomplete", autocomplete);

})();

(function () {
    'use strict';

    angular.module("ngInput", [
        "ngCommon",
        "ngInput.directives"
    ]);

})();