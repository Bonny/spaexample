/**
 *  Commons components
 *  @version 1.0
 */

/**
 * ####### FILTERS #######
 */

(function () {
    'use strict';

    var range = function () {
        return function (input, total) {
            total = parseInt(total);
            for (var i = 0; i < total; i++)
                input.push(i);
            return input;
        };
    };

    var purger = function () {
        return function (input) {
            return input ? input.replace(/[\\$\\^]/g, "") : "";
        };
    };

    var truncateText = function () {
        return function (text) {
            if (text && text.length > 20 && text.indexOf(" ") === -1 && text.indexOf("/") > -1) {
                var arrayText = text.split("/");
                text = arrayText[0] + " / " + arrayText[1];
            }
            return text;
        };
    };
    
    var trusted = ['$sce', function ($sce) {
            return function (html) {
                return $sce.trustAsHtml(html);
            };
        }];
    
    angular.module("ngCommon.filter", [])
            .filter("range", range)
            .filter("purger", purger)
            .filter('truncateText', truncateText)
            .filter('trusted', trusted);

})();

/**
 * ####### SERVICES #######
 */

(function () {
    'use strict';

    var $debounce = ['$rootScope', '$browser', '$q', '$exceptionHandler', function ($rootScope, $browser, $q, $exceptionHandler) {
            var deferreds = {},
                    methods = {},
                    uuid = 0;

            function debounce(fn, delay, invokeApply) {
                var deferred = $q.defer(),
                        promise = deferred.promise,
                        skipApply = (angular.isDefined(invokeApply) && !invokeApply),
                        timeoutId, cleanup,
                        methodId, bouncing = false;
                delay = delay || 300;
                // check we dont have this method already registered
                angular.forEach(methods, function (value, key) {
                    if (angular.equals(methods[key].fn, fn)) {
                        bouncing = true;
                        methodId = key;
                    }
                });

                // not bouncing, then register new instance
                if (!bouncing) {
                    methodId = uuid++;
                    methods[methodId] = {fn: fn};
                } else {
                    // clear the old timeout
                    deferreds[methods[methodId].timeoutId].reject('bounced');
                    $browser.defer.cancel(methods[methodId].timeoutId);
                }

                var debounced = function () {
                    // actually executing? clean method bank
                    delete methods[methodId];
                    try {
                        deferred.resolve(fn());
                    } catch (e) {
                        deferred.reject(e);
                        $exceptionHandler(e);
                    }
                    if (!skipApply)
                        $rootScope.$apply();
                };
                timeoutId = $browser.defer(debounced, delay);
                // track id with method
                methods[methodId].timeoutId = timeoutId;
                cleanup = function (reason) {
                    delete deferreds[promise.$$timeoutId];
                };
                promise.$$timeoutId = timeoutId;
                deferreds[timeoutId] = deferred;
                promise.then(cleanup, cleanup);

                return promise;
            }
            // similar to angular's $timeout cancel
            debounce.cancel = function (promise) {
                if (promise && promise.$$timeoutId in deferreds) {
                    deferreds[promise.$$timeoutId].reject('canceled');
                    return $browser.defer.cancel(promise.$$timeoutId);
                }
                return false;
            };

            return debounce;
        }];

    var $utils = [function () {
            // default
            var ex = {code: "UNKNOW", reason: "unknow-error", status: 500};
            return {
                getError: function (response) {
                    return response ? angular.extend(ex, response) : ex;
                },
                isNullOrEmpty: function (value) {
                    return this.count(value) === 0;
                },
                count: function (value) {
                    return (value && angular.isDefined(value) ? value.length : 0);
                },
                isPlainText: function (value) {
                    return /^(\w*|\s*)[\w\s]*(\w*|\s*)$/g.test(value);
                },
                isNumber: function (value) {
                    return /^\d*[1-9]\d*$/g.test(value) || /^-\d*[1-9]\d*$/g.test(value) || /0/g.test(value);
                },
                isEmail: function (value) {
                    return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/g.test(value);
                },
                isBool: function (b) {
                    return (/^true$/i).test(b);
                },
                hexToRgb: function (hex) {
                    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
                    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
                    hex = hex.replace(shorthandRegex, function (m, r, g, b) {
                        return r + r + g + g + b + b;
                    });
                    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                    return result ? [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)] : [0, 0, 0];
                },
                isLight: function (hex) {
                    var rgb = this.hexToRgb(hex);
                    return (0.213 * rgb[0] + 0.715 * rgb[1] + 0.072 * rgb[2] > 255 / 2);
                },
                transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                replaceAll: function (find, replace, str) {
                    return str.replace(new RegExp(find, 'g'), replace);
                }
            };
        }];

    var $logger = ['$log', function ($log) {
            var store = [], current = [];
            function log(level, msg) {
                current.push(msg);
                store.push(msg);
                switch (level) {
                    case "INFO":
                        $log.info(msg);
                        break;
                    case "WARN":
                        $log.warn(msg);
                        break;
                    case "DEBUG":
                        $log.debug(msg);
                        break;
                    case "ERROR":
                        $log.error(msg);
                        break;
                }
            }
            return {
                info: function (msg) {
                    log("INFO", msg);
                },
                warn: function (msg) {
                    log("WARN", msg);
                },
                debug: function (msg) {
                    log("DEBUG", msg);
                },
                error: function (msg) {
                    log("ERROR", msg);
                },
                getAll: function () {
                    return angular.copy(store);
                }
            };
        }];

    var $toast = ['$rootScope', function ($rootScope) {
            return {
                danger: function (msg) {
                    $rootScope.$broadcast('$ToastDanger', msg);
                },
                success: function (msg) {
                    $rootScope.$broadcast('$ToastSuccess', msg);
                },
                warning: function (msg) {
                    $rootScope.$broadcast('$ToastWarning', msg);
                }
            };
        }];

    var $exceptionHandler = ['$log', '$injector', 'urls', 'info', function ($log, $injector, urls, info) {
            var logErrorToServerSide = function (exception, cause) {
                try {
                    var requestData = {
                        frontendUrl: $injector.get('$window').location.href,
                        message: exception.toString(),
                        type: "exception",
                        stack: exception.stack,
                        cause: cause,
                        clientId: info.name,
                        clientVersion: info.version
                    };
                    $injector.get('$http')
                            .post(urls.logflush, requestData, {"isNotBlockingActivity": true})
                            .catch(function (response) {
                                $log.error('Cannot log error to server side - connection problem.');
                            });
                } catch (e) {
                    $log.error('Cannot log error to server side.');
                    $log.log(e);
                }
            };
            return function (exception, cause) {
                $log.error.apply($log, arguments);
                logErrorToServerSide(exception, cause);
            };
        }];

    angular.module("ngCommon.service", [])
            .factory("$debounce", $debounce)
            .factory("$utils", $utils)
            .factory("$logger", $logger)
            .factory("$toast", $toast)
            .factory('$exceptionHandler', $exceptionHandler);

})();

/**
 * ####### DIRECTIVE #######
 */

(function () {
    'use strict';

    var error = [function () {
            return {
                restrict: 'E',
                transclude: true,
                scope: {
                    text: '='
                },
                template: '<div class="text-danger">' +
                        '   <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>' +
                        '   <p ng-if="text">{{text|translate}}</p>' +
                        '  </div>'
            };
        }];

    var loading = [function () {
            return {
                restrict: 'E',
                transclude: true,
                scope: {
                    size: '@',
                    text: '@'
                },
                template: '<i class="fa fa-spinner fa-pulse" ng-class="{\'fa-2x\': size && size === \'large\'}" aria-hidden="true"></i>' +
                        '<p ng-if="text && text === \'true\'" translate>loading</p>'
            };
        }];

    var sortable = [function () {
            return {
                restrict: 'A',
                transclude: true,
                scope: {
                    order: '=',
                    sort: '=',
                    callbackFn: '&'
                },
                template: '<a ng-click="sort_by(order)" style="color: #555555;" class="no-underline">' +
                        '    <span ng-transclude></span>' +
                        '    <i ng-class="selectedCls(order)"></i>' +
                        '</a>',
                link: function (scope, element, attrs) {
                    scope.sort_by = function (newProperty) {
                        var sort = scope.sort;
                        if (!sort.direction) {
                            sort.direction = 'DESC';
                        }
                        if (sort.property == newProperty) {
                            sort.direction = (sort.direction == 'DESC') ? 'ASC' : 'DESC';
                        }
                        sort.property = newProperty;
                        scope.callbackFn();
                    };
                    scope.selectedCls = function (column) {
                        if (column == scope.sort.property) {
                            return ('fa fa-chevron-' + ((scope.sort.direction == 'DESC') ? 'down' : 'up'));
                        } else {
                            return'fa fa-sort';
                        }
                    };
                }
            };
        }];

    var toast = ['$timeout', function ($timeout) {
            return {
                restrict: 'E',
                scope: {},
                template: '<div style="position:absolute; left:50%; top: 5%; width:600px; margin-left:-300px; z-index:20000; display: block;">' +
                        '    <div ng-repeat="a in alerts" id="alert-{{a.id}}" class="alert alert-{{a.type}}" role="alert" style="margin-bottom: 6px;">' +
                        '       <a ng-click="removeAlert(a.id)" class="close link-hover" data-dismiss="alert" aria-label="close">&times;</a>' +
                        '       <p>{{a.text}}</p>' +
                        '    </div>' +
                        '  </div>',
                link: function (scope, element, attrs) {
                    scope.alerts = [];
                    scope.alertCount = 0;
                    scope.removeAlert = function (alertId) {
                        $("#alert-" + alertId).fadeOut(1000, function () {
                            $(this).remove();
                        });
                    };
                    var addAlert = function (type, text) {
                        var alertId = scope.alertCount++;
                        scope.alerts.push({id: alertId, type: type, text: text});
                        $timeout(function () {
                            scope.removeAlert(alertId);
                        }, type === "success" ? 4000 : 10000);
                    };
                    scope.$on("$ToastSuccess", function (event, msg) {
                        addAlert('success', msg);
                    });
                    scope.$on("$ToastDanger", function (event, msg) {
                        addAlert('danger', msg);
                    });
                    scope.$on("$ToastWarning", function (event, msg) {
                        addAlert('warning', msg);
                    });
                }
            };
        }];


    angular.module("ngCommon.directive", [])
            .directive("error", error)
            .directive("loading", loading)
            .directive("sortable", sortable)
            .directive("toast", toast);

})();

/**
 * ####### MODULE #######
 */

(function () {
    'use strict';

    angular.module("ngCommon", [
        "ngCommon.service",
        "ngCommon.filter",
        "ngCommon.directive"
    ]);

})();