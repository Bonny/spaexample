/**
 * Grid module
 */

var FilterOperator = {
    LIKE: "LIKE", NOT_LIKE: "NOT_LIKE", IN: "IN", NOT_IN: "NOT_IN", EQUALS: "EQUALS", NOT_EQUALS: "NOT_EQUALS",
    GREATER: "GREATER", GREATER_AND_EQUALS: "GREATER_AND_EQUALS",
    LESS: "LESS", LESS_AND_EQUALS: "LESS_AND_EQUALS",
    BETWEEN: "BETWEEN", NOT_BETWEEN: "NOT_BETWEEN"
};

var FilerView = {
    TEXT: 'text',
    DATE: 'date',
    COMBO: 'combo',
    NUMBER: 'number',
    EMAIL: 'email'
};

var FilerType = {
    TEXT: 'TEXT',
    DATE: 'DATE',
    NUMBER: 'NUMBER'
};

function ComboValues(header, value) {
    this.header = header;
    this.value = value;
}

function Filterable(view, type, operator, values) {
    /**
     * FilerView
     */
    this.view = view;
    /**
     * FilerType
     */
    this.type = type;
    /**
     * FilterOperator
     */
    this.operator = operator;
    /**
     * ComboValues[]
     */
    this.values = values || [];
}

function FilterableText() {
    return new Filterable(FilerView.TEXT, FilerType.TEXT, FilterOperator.LIKE);
}

function FilterableNumber() {
    return new Filterable(FilerView.NUMBER, FilerType.NUMBER, FilterOperator.EQUALS);
}

function FilterableDate() {
    return new Filterable(FilerView.DATE, FilerType.DATE, FilterOperator.BETWEEN);
}

/**
 * ####### DIRECTIVES #######
 */

(function () {
    'use strict';

    var fCombo = [function () {
            return {
                restrict: 'E',
                transclude: true,
                template: '<select ng-model="filter[c.dataIndex]"' +
                        '    ng-init="filter[c.dataIndex] = getFilterDefVal(c)"' +
                        '    ng-change="refresh()"' +
                        '    name="fin-{{c.dataIndex}}"' +
                        '    class="form-control form-control-sm">' +
                        '      <option ng-repeat="v in c.filterable.values" ng-value="v.value">{{v.header|translate}}</option>' +
                        '  </select>'
            };
        }];

    var fText = [function () {
            return {
                restrict: 'E',
                transclude: true,
                template: '<input ng-model="filter[c.dataIndex]"' +
                        '    ng-blur="filterBlur(c)"' +
                        '    ng-change="filterStatus[c.dataIndex] = true"' +
                        '    ng-focus="filterStatus[c.dataIndex] = false"' +
                        '    ng-class="{\'invalid-input\':!filterValidator[c.dataIndex]}"' +
                        '    name="fin-{{c.dataIndex}}"' +
                        '    type="text" class="form-control form-control-sm">' +
                        '  <!--small class="form-text text-muted">Valore non valido</small-->'
            };
        }];

    var fDate = [function () {
            return {
                restrict: 'E',
                transclude: true,
                scope: {
                    validator: '=',
                    model: '=',
                    onChange: '&'
                },
                template:
                        '<button id="btn-{{id}}" ng-click="toogle()" ng-class="{\'invalid-input\':!(validator=!date.invalid)}" type="button" class="btn btn-sm btn-outline-secondary">' +
                        '{{from()}} {{to()}}' +
                        '</button>' +
                        '<div class="card" id="card-{{id}}" style="visibility:hidden;width:24rem;z-index:99999;position:absolute;">' +
                        '    <div class="card-body">' +
                        '      <date-picker-range value="date"></date-picker-range>' +
                        '    </div>' +
                        '</div>'
                ,
                link: function (scope, element, attrs) {
                    scope.id = new Date().getTime();
                    scope.date = {
                        from: 0,
                        to: 0,
                        invalid: false
                    };

                    function format(ts) {
                        var f = {dd: "--", mm: "--", yy: "----"};
                        if (ts > 0) {
                            var date = moment(ts);
                            f.dd = date.get("date");
                            f.mm = parseInt(date.get("month")) + 1;
                            f.yy = date.get("year");
                        }
                        //TODO: add pattern i18n
                        return (f.dd + "/" + f.mm + "/" + f.yy);
                    }
                    scope.to = function () {
                        return format(scope.date.to);
                    };
                    scope.from = function () {
                        return format(scope.date.from);
                    };
                    var startup = false;
                    scope.up = true;
                    scope.toogle = function () {
                        if (!startup) {
                            //TODO: FIX 
                            $('#card-' + scope.id).css("visibility", "visible");
                            startup = true;
                        }
                        if (scope.up) {
                            $('#card-' + scope.id).slideDown({
                                duration: 250,
                                complete: function () {
                                    $('#btn-' + scope.id).button('toggle');
                                }
                            });
                            scope.up = false;
                        } else {
                            $('#card-' + scope.id).slideUp({
                                duration: 250,
                                complete: function () {
                                    $('#btn-' + scope.id).button('toggle');
                                }
                            });
                            scope.up = true;
                            if (!scope.date.invalid) {
                                var m = scope.model ? JSON.stringify(scope.model) : "";
                                var d = JSON.stringify(scope.date);
                                if (d !== m) {
                                    scope.model = angular.copy(scope.date);
                                    scope.onChange();
                                }
                            }
                        }
                    };
                }
            };
        }];

    var grid = ['$utils', '$debounce', '$http', function ($utils, $debounce, $http) {
            return {
                restrict: 'E',
                transclude: true,
                scope: {
                    config: '='
                },
                templateUrl: './partials/grid.html',
                link: function (scope, element, attrs) {
                    
                    scope.debug = false;
                    scope.limits = [10, 20, 30, 50];
                    scope.sorter = {property: '', direction: ''};
                    scope.range = {limit: 20, offset: 1};
                    scope.filter = {};
                    scope.total = 0;
                    scope.items = [];
                    scope.loading = false;
                    scope.error = false;
                    scope.errorMsg = '';

                    scope.getItems = function () {
                        return scope.items;
                    };

                    // check column //
                    var setIndeterminate = function (check) {
                        $("#check-all").prop("indeterminate", check);
                        //document.getElementById("check-all").indeterminate = check;  
                    };
                    scope.checkAll = {
                        isCheck: false
                    };
                    scope.checkAll = function () {
                        for (var i = 0; i < $utils.count(scope.items); i++) {
                            scope.items[i].checked = scope.checkAll.isCheck;
                        }
                        if (!scope.checkAll.isCheck) {
                            setIndeterminate(false);
                        }
                    };
                    scope.changeCheck = function (event, r) {
                        event.stopPropagation();
                        var totCheck = scope.getNumRowChecked();
                        var totItems = $utils.count(scope.getItems());
                        scope.checkAll.isCheck = r.checked ? totItems === totCheck : false;
                        setIndeterminate(!scope.checkAll.isCheck && totCheck > 0);
                    };
                    var getRowChecked = function () {
                        var checked = [];
                        for (var i = 0; i < $utils.count(scope.items); i++) {
                            if (scope.items[i].checked) {
                                checked.push(scope.items[i]);
                            }
                        }
                        return checked;
                    };
                    scope.getNumRowChecked = function () {
                        return $utils.count(getRowChecked());
                    };

                    // props and utils //

                    var _is = function (p) {
                        return scope.config[p] || false;
                    };
                    scope.displayBtnLabel = function () {
                        return _is('displayBtnLabel');
                    };
                    scope.isHover = function () {
                        return _is('hover');
                    };
                    scope.isStriped = function () {
                        return _is('striped');
                    };
                    scope.isSelectable = function () {
                        return _is('selectable');
                    };
                    scope.getColspan = function () {
                        var cols = $utils.count(scope.getColumns());
                        cols += 1;//for actions
                        if (scope.isSelectable())
                            cols += 1;
                        return cols;
                    };
                    scope.getColumns = function () {
                        return scope.config.columns || [];
                    };

                    scope.getToolbar = function () {
                        return scope.config.toolbar || [];
                    };


                    // actions //

                    scope.getActions = function () {
                        return scope.config.actions || [];
                    };
                    scope.fireAction = function (act, row) {
                        act.callbackFn([row]);
                    };
                    scope.fireActionGroup = function (act) {
                        act.callbackFn(getRowChecked());
                    };

                    // filetr //

                    scope.filterValidator = {};
                    scope.filterStatus = {};

                    if (scope.config.columns) {
                        for (var i in scope.config.columns) {
                            var index = scope.config.columns[i].dataIndex;
                            scope.filterValidator[index] = true;
                            scope.filterStatus[index] = false;
                            scope.filter[index] = null;
                        }
                    }

                    scope.filterBlur = function (c) {
                        var value = scope.filter[c.dataIndex];
                        var rs = true;
                        if (value) {
                            var view = c.filterable.view;
                            switch (view) {
                                case FilerView.NUMBER:
                                    rs = $utils.isNumber(value);
                                    break;
                                case FilerView.TEXT:
                                    rs = $utils.isPlainText(value);
                                    break;
                                case FilerView.EMAIL:
                                    rs = $utils.isEmail(value);
                                    break;
                                    //for DATE see fDate
                            }
                        }
                        scope.filterValidator[c.dataIndex] = rs;
                        if (rs && scope.filterStatus[c.dataIndex]) {
                            scope.filterStatus[c.dataIndex] = false;
                            scope.refresh();
                        }
                    };

                    scope.getFilterDefVal = function (c) {
                        var i = 0, found = false;
                        while (i < $utils.count(c.filterableValues) && !found) {
                            if (c.filterableValues[i]["def"])
                                found = true;
                            else
                                i++;
                        }
                        return (found) ? c.filterableValues[i].value : null;
                    };

                    // paginator //

                    scope.next = function () {
                        scope.range.offset++;
                        scope.refresh();
                    };
                    scope.prev = function () {
                        scope.range.offset--;
                        scope.refresh();
                    };
                    scope.numberOfPages = function () {
                        return Math.ceil(scope.total / scope.range.limit);
                    };
                    scope.changeLimit = function (limit) {
                        scope.range.limit = limit;
                        scope.refresh();
                    };

                    // refresh method //

                    scope.refresh = function () {
                        $debounce(_refresh);
                    };

                    //private
                    function Field(dataIndex, value, type, operator) {
                        this.dataIndex = dataIndex;
                        this.value = value;
                        this.type = type;
                        this.operator = operator;
                    }

                    var _refresh = function () {
                        var paged = {range: scope.range, sorter: scope.sorter, filter: {relation: 'AND', fields: []}};
                        for (var i = 0; i < $utils.count(scope.getColumns()); i++) {
                            var c = scope.config.columns[i];
                            var index = c.dataIndex;
                            var value = scope.filter[index];
                            if (scope.filterValidator[index] && value) {
                                var f = c.filterable;
                                paged.filter.fields.push(new Field(index, value, f.type, f.operator));
                            }
                        }

                        scope.loading = true;
                        scope.error = false;
                        scope.items = [];
                        scope.total = 0;
                        scope.checkAll.isCheck = false;
                        scope.checkAll();

                        var success = function (res) {
                            scope.loading = false;
                            scope.items = res.items;
                            scope.total = res.total;
                        };

                        var error = function (res) {
                            var ex = $utils.getError(res);
                            scope.loading = false;
                            scope.error = true;
                            scope.errorMsg = ex.reason;
                        };

                        $http.get(scope.config.url, {params: {paged: paged}}).success(success).error(error);
                    };

                    scope.config.update = function () {
                        scope.refresh();
                    };

                    scope.refresh();
                }
            };
        }];

    angular.module("ngGrid.directives", [])
            .directive("fCombo", fCombo)
            .directive("fText", fText)
            .directive("fDate", fDate)
            .directive("grid", grid)
            ;

})();

(function () {
    'use strict';

    angular.module("ngGrid", [
        "ngCommon",
        "ngGrid.directives"
    ]);

})();