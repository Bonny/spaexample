/**
 * Auth module
 */


/**
 * ####### DIRECTIVES #######
 */

(function () {
    'use strict';

    var signin = ['$auth', '$logger', '$state', '$timeout', '$utils',
        function ($auth, $logger, $state, $timeout, $utils) {
            return {
                restrict: 'E',
                transclude: true,
                scope: {},
                template: '<div class="card mx-auto" style="width: 20rem; margin-top: 2rem;">' +
                        '    <div class="card-body">' +
                        '      <form role="form" ng-submit="signin()">' +
                        '        <div class="form-group">' +
                        '          <label for="username">Username</label>' +
                        '          <input type="text" class="form-control" id="username" ng-model="username" ng-focus="fail = false">' +
                        '        </div>' +
                        '        <div class="form-group">' +
                        '          <label for="password">Password</label>' +
                        '          <input type="password" class="form-control" id="password" ng-model="password" ng-focus="fail = false">' +
                        '        </div>' +
                        '        <div style="margin: 1em 0 1em 0;">' +
                        '          <div class="alert alert-success text-center" ng-if="success" role="alert">' +
                        '            <span translate>signin-success</span>' +
                        '          </div>' +
                        '          <div class="alert alert-danger text-center" ng-if="fail" role="alert">' +
                        '            <div ng-switch on="fail">' +
                        '              <span ng-switch-when="auth-invalid-credential" translate>auth-invalid-credential</span>' +
                        '              <span ng-switch-when="auth-profile-suspended" translate>auth-profile-suspended</span>' +
                        '              <span ng-switch-when="auth-not-authorized" translate>auth-not-authorized</span>' +
                        '              <span ng-switch-default translate>unknow-error</span>' +
                        '            </div>' +
                        '          </div>' +
                        '        </div>' +
                        '        <button type="submit" ng-disabled="loading || (!username || !password)" ng-click="fail = false" class="btn btn-primary btn-block">' +
                        '          <loading ng-if="loading"></loading>' +
                        '          <span translate>signin</span>' +
                        '        </button>' +
                        '      </form>' +
                        '    </div>' +
                        '</div>',
                link: function (scope) {
                    $logger.debug("SignCtrl.load");
                    scope.loading = false;
                    scope.success = false;
                    scope.fail = false;
                    scope.username = scope.password = "admin";
                    var success = function () {
                        $logger.debug("SignCtrl.success");
                        scope.loading = false;
                        scope.fail = false;
                        scope.success = true;
                        $timeout(function () {
                            $logger.debug("SignCtrl go -> index");
                            scope.success = false;
                            $state.go('index');
                        }, 500);
                    };
                    var fail = function (response) {
                        $logger.debug("SignCtrl.fail");
                        scope.loading = false;
                        scope.success = false;
                        scope.fail = $utils.getError(response).reason;
                    };
                    scope.signin = function () {
                        $logger.debug("SignCtrl.signin");
                        scope.loading = true;
                        $auth.signin({username: scope.username, password: scope.password}, success, fail);
                    };
                }
            };
        }];

    angular.module("ngAuth.directives", [])
            .directive("signin", signin);

})();

(function () {
    'use strict';

    var $httpInterceptor = ['$q', '$localStorage', '$logger', '$rootScope', 'urls', '$utils',
        function ($q, $localStorage, $logger, $rootScope, urls, $utils) {

            function isRestful(config) {
                return (config && config.url && config.url.indexOf(urls.baseUrl) === 0);
            }

            function logRequest(config) {
                config.requestTimestamp = new Date().getTime();
                config.requestID = config.requestTimestamp;
                $logger.debug("[" + config.requestID + "] Request [" + config.method + "] " + config.url);
                if (isRestful(config))
                    config.url = config.url + "?_rID=" + config.requestTimestamp;
                else
                    config.url = config.url + "?v=" + config.requestTimestamp;
            }

            function logResponse(config, error) {
                var info = "[" + config.requestID + "] Response to [" + config.method + "] " + config.url + " request in " + (new Date().getTime() - config.requestTimestamp) + "ms";
                if (!error)
                    $logger.debug(info);
                else
                    $logger.error(info + " - " + JSON.stringify($utils.getError(error)));
            }

            return {
                request: function (config) {
                    logRequest(config);
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers['Authorization'] = 'Bearer ' + $localStorage.token;
                    }
                    config.headers['Access-Control-Allow-Origin'] = '*';
                    config.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS, HEAD';
                    config.headers['Access-Control-Allow-Headers'] = 'origin, content-type, accept, authorization';
                    return config;
                },
                response: function (response) {
                    var config = response.config;
                    logResponse(config);
                    return response;
                },
                responseError: function (response) {
                    var config = response.config;
                    logResponse(config, response.data || {});
                    if (response.status === 401) {
                        delete $localStorage.token;
                        $rootScope.$broadcast('$TokenExpired');
                    }
                    return $q.reject(response);
                }
            };
        }];

    var $auth = ['$http', '$localStorage', 'urls', function ($http, $localStorage, urls) {

            function urlBase64Decode(str) {
                var output = str.replace('-', '+').replace('_', '/');
                switch (output.length % 4) {
                    case 0:
                        break;
                    case 2:
                        output += '==';
                        break;
                    case 3:
                        output += '=';
                        break;
                    default:
                        throw 'Illegal base64url string!';
                }
                return window.atob(output);
            }

            function getClaimsFromToken() {
                var token = $localStorage.token;
                var user = {};
                if (typeof token !== 'undefined') {
                    var encoded = token.split('.')[1];
                    user = JSON.parse(urlBase64Decode(encoded));
                }
                return user;
            }

            var tokenClaims = getClaimsFromToken();
            var user = tokenClaims ? angular.fromJson(tokenClaims.user) : {};

            return {
                signin: function (data, success, error) {
                    $http.post(urls.auth, data).success(function (res) {
                        $localStorage.token = res.token;
                        tokenClaims = getClaimsFromToken();
                        user = tokenClaims ? angular.fromJson(tokenClaims.user) : {};
                        success();
                    }).error(error);
                },
                logout: function (success) {
                    tokenClaims = {};
                    user = {};
                    delete $localStorage.token;
                    if (success)
                        success();
                },
                getTokenClaims: function () {
                    return tokenClaims;
                },
                isAuth: function () {
                    return $localStorage.token ? true : false;
                },
                getUser: function () {
                    return user;
                }
            };
        }];

    angular.module("ngAuth.service", [])
            .factory("$httpInterceptor", $httpInterceptor)
            .factory("$auth", $auth);

})();

(function () {
    'use strict';

    angular.module("ngAuth", [
        //ext
        "ui.router",
        "ngStorage",
        //local
        "ngCommon",
        "ngAuth.service",
        "ngAuth.directives"
    ]);

})();