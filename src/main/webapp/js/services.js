(function () {
    'use strict';

    angular.module('app').factory('$config', ['$http', 'urls', '$utils', function ($http, urls, $utils) {
            var init = false;
            var config = {};
            return {
                init: function (success) {//TODO: gestire errore
                    $http.get(urls.config).success(function (res) {
                        init = true;
                        config = angular.extend(config, res);
                        success();
                    });
                },
                isInit: function () {
                    return init;
                },
                getConfig: function () {
                    return angular.extend({}, config);
                }
            };
        }
    ]);

})();