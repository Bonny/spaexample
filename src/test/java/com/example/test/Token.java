package com.example.test;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import java.security.Key;
import java.util.Date;
import org.junit.Test;

/**
 *
 * @author luca
 */
public class Token {

    @Test
    public void test() {
        
        final SignatureAlgorithm SAlg = SignatureAlgorithm.HS512;
        final Key key = MacProvider.generateKey();

        long expMillis = 5000l;
        Date expirationTime = new Date(expMillis);

        String compact = Jwts.builder().setSubject("Joe").setExpiration(expirationTime).signWith(SAlg, key).compact();
        System.out.println("compact : " + compact);
        try {
            String unpack = Jwts.parser().setSigningKey(key).parseClaimsJws(compact).getBody().getSubject();
            System.out.println("unpackage 0 : " + unpack);

            // check if the expiration work.
            Thread.sleep(3000);
            System.out.println("unpackage 1 : " + Jwts.parser().setSigningKey(key).parseClaimsJws(compact).getBody().getSubject());

            //Create a *new* token that reflects a longer extended expiration time.
            Date date1 = new Date();
            long t1 = date1.getTime();
            Date expirationTime1 = new Date(t1 + 5000l); //prolongation 5 seconds

            String compact2 = Jwts.builder().setSubject("Joe").setExpiration(expirationTime1).signWith(SAlg, key).compact();

            // check if the extend expiration work.
            Thread.sleep(3000);
            System.out.println("unpackage 2 : " + Jwts.parser().setSigningKey(key).parseClaimsJws(compact2).getBody().getSubject());

            Thread.sleep(1000);
        } catch (InterruptedException | ExpiredJwtException ex) {
            System.out.println("exception : " + ex.getMessage());
            System.out.println(ex.getClass());
            Thread.currentThread().interrupt();
        }

    }
}
